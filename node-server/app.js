const http = require('http');
var mysql = require('mysql');
var conn = mysql.createConnection({ host : "localhost", user : "asjuan", password : "asjuan", database : "asjuan" });
conn.connect();
let handlers = {};

handlers["/login"] = (req, res) => {
	console.log("signing in........");
	let d = "";
	req.on('data', (dd) => {
		d += dd.toString();
	});
	req.on('end', () => {
		let obj = JSON.parse(d);
		let juansql = "SELECT * FROM juan WHERE email = ? AND password = ?;";
		let clientsql = "SELECT * FROM client WHERE email = ? AND password = ?;";
		loginCheckIfJuan(juansql, obj.email, obj.password, (juanresult) => {
			if(juanresult == "yes") {
				res.writeHead(200, { "Content-Type" : "application/json" });
				res.end(JSON.stringify({result : "Success", message : "Login Success!", activity : "Login", usertype : "JUAN"}));
			}
			else if(juanresult == "no") {
				loginCheckIfClient(clientsql, obj.email, obj.password, (clientresult) => {
					if(clientresult == "yes") {
						res.writeHead(200, { "Content-Type" : "application/json" });
						res.end(JSON.stringify({result : "Success", message : "Login Success!", activity : "Login", usertype : "CLIENT"}));
					}
					else if(clientresult == "no") {
						res.writeHead(200, { "Content-Type" : "application/json" });
						res.end(JSON.stringify({result : "Failed", message : "Please enter your email and/or password again.", activity : "Login"}));
					}
				});
			}
		});
	});
}

loginCheckIfJuan = (sql, email, password, callback) => {
	conn.query(sql, [email, password], (err, rows, fields) => {
		if(rows == "") {
			callback("no");
		}
		else {
			callback("yes");
		}
	});
};

loginCheckIfClient = (sql, email, password, callback) => {
	conn.query(sql, [email, password], (err, rows, fields) => {
		if(rows == "") {
			callback("no");
		}
		else {
			callback("yes");
		}
	});
};

handlers["/register"] = (req, res) => {
	console.log("registering......");
	if(req.method == "POST") {
		let d = "";
		req.on('data', (dd) => {
			d += dd.toString();
		});
		req.on('end', () => {
			let obj = JSON.parse(d);
			console.log(obj.usertype);
			let sql = "";
			let tobeinsertedsql = "";
			if(obj.usertype == "CLIENT") {
				sql = "SELECT * FROM client WHERE email = ?;";
				tobeinsertedsql = "INSERT INTO client SET ?";
			}
			else if(obj.usertype == "JUAN") {
				sql = "SELECT * FROM juan WHERE email = ?;";
				tobeinsertedsql = "INSERT INTO juan SET ?";
			}
			conn.query(sql, [obj.email], (err, rows, fields) => {
				if(rows == "") {
					var values = {"email":obj.email, "fullname":obj.fullname, "contact_number":"+63" + obj.contact, "password":obj.password, "profile_picture":"default.jpg"};
					conn.query(tobeinsertedsql, values, (err, results, fields) => {
						if(err) {
							res.end(JSON.stringify({result : err}));
						}
						else {
							res.end(JSON.stringify({result : "Success", message : "Registration Success!", activity : "Registration"}));
						}
					});
				}
				else {
					res.end(JSON.stringify({result : "Failed", message : "Registration Failed!", activity : "Registration"}));
				}
			});
		});
	}
};

http.createServer((req, res) => {
	if(handlers[req.url]) {
		handlers[req.url](req, res);
	}
	else {
		res.writeHead(404, { "Content-Type" : "application/json" });
		res.end(JSON.stringify({ error : "Failed to load" }));
	}
}).listen(8080);

console.log("Server is running");
