package com.asjuan;

import android.os.AsyncTask;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import android.util.JsonWriter;
import org.json.JSONObject;
import android.content.Context;
import android.widget.Toast;
import android.content.Intent;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class HttpClient extends AsyncTask<String, Void, String>
{
	private String api;
	private Context ctx;

	public HttpClient(String a, Context c) {
		api = a;
		ctx = c;
	}

	public String doInBackground(String... str) {
		String rData = "";
		StringBuilder sb = new StringBuilder("");
		try {
			URL url = new URL(api);
			HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
			urlConnection.setRequestMethod("POST");
			urlConnection.setDoOutput(true);
			urlConnection.setChunkedStreamingMode(0);
			OutputStream os = new BufferedOutputStream(urlConnection.getOutputStream());
			JsonWriter jw = new JsonWriter(new OutputStreamWriter(os, "UTF-8"));
			switch(str[0]) {
				case "register":
					String[] registerFieldNames = {"", "fullname", "email", "contact", "password", "usertype"};
					jw.beginObject();
					for(int i=1; i<str.length; i++) {
						jw.name(registerFieldNames[i]).value(str[i]);
					}
					jw.endObject();
					jw.close();
					break;
				case "login":
					String[] loginFieldNames = {"", "email", "password"};
					jw.beginObject();
					for(int i=1; i<str.length; i++) {
						jw.name(loginFieldNames[i]).value(str[i]);
					}
					jw.endObject();
					jw.close();
					break;
			}
			rData = GetResponseFromServer.getResponse(urlConnection);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return(rData);
	}

	public void onPostExecute(String result) {
		try {
			JSONObject obj = new JSONObject(result);
			String r = (String)obj.get("result");
			String message = (String)obj.get("message");
			String act = (String)obj.get("activity");
			if("Success".equals(r)) {
				switch(act) {
					case "Registration":
						Intent signIn = new Intent(ctx, SignIn.class);
						ctx.startActivity(signIn);
						break;
					case "Login":
						switch((String)obj.get("usertype")) {
							case "JUAN":
								Intent homejuan = new Intent(ctx, HomeJuanActivity.class);
								ctx.startActivity(homejuan);
								break;
							case "CLIENT":
								Intent homeclient = new Intent(ctx, HomeClientActivity.class);
								ctx.startActivity(homeclient);
								break;
						}
				}
			}
			else if("Failed".equals(r)) {
				switch(act) {
					case "Registration":
						Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
					case "Login":
						AlertDialog.Builder adb = new AlertDialog.Builder(ctx);
						adb.setTitle("Incorrect Email and/or Password").setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});
						AlertDialog ad = adb.create();
						ad.show();
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
