package com.asjuan;

public class FieldValidator
{
	public static boolean isFullnameValid(String fullname) {
		boolean result = false;
		if(fullname.length() < 8 || fullname.length() > 35 ) {
			result = false;
		}
		else {
			result = true;
		}
		return(result);
	}

	public static boolean isEmailValid(String email) {
		boolean result = false;
		boolean at = false;
		boolean period = false;
		if(email.length() != 0) {
			if(email.charAt(email.length()-1) == '@') {
				at = true;
				period = false;
			}
			else {
				if(email.contains("@")) {
					at = true;
					String[] p = email.split("@");
					if(p[1].contains(".")) {
						period = true;
					}
				}
			}
		}
		if(at == true && period == true) {
			result = true;
		}
		return(result);
	}

	public static boolean isNumberValid(String number) {
		boolean result = false;
		boolean first = false;
		boolean len = true;
		if(number.length() != 0) {
			if(number.charAt(0) == '9') {
				first = true;
			}
		}
		if(number.length() == 10) {
			len = true;
		}
		if(first == true && len == true) {
			result = true;
		}
		return(result);
	}

	public static boolean isPasswordValid(String password) {
		boolean result = false;
		boolean alphanumericwithspecial = false;
		boolean len = false;
		if(!password.matches("[A-Za-z0-9]*")) {
			alphanumericwithspecial = true;
		}
		if(password.length() < 8 || password.length() > 16) {
			len = true;
		}
		if(alphanumericwithspecial == true && len == false) {
			result = true;
		}
		return(result);
	}

	public static boolean isConfirmPasswordCorrect(String pass, String cpass) {
		boolean result = false;
		if(pass.equals(cpass)) {
			result = true;
		}
		return(result);
	}
}
