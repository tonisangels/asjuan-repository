package com.asjuan;

import android.os.Bundle;
import android.content.Intent;
import android.widget.TextView;
import android.view.View;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.support.annotation.Nullable;

public class ContactUs extends Fragment
{
	View view;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.contactus, container, false);
		TextView text = (TextView)view.findViewById(R.id.contact_text);
		TextView name = (TextView)view.findViewById(R.id.contact_fullname);
		TextView email = (TextView)view.findViewById(R.id.contact_email);
		TextView message = (TextView)view.findViewById(R.id.contact_message);
		return(view);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getActivity().setTitle("CONTACT US");
	}
}
