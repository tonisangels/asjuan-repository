package com.asjuan;

import android.app.Service;
import android.content.Context;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View.OnClickListener;

public class InternetChecker {

	private Context context;
	private AlertDialog.Builder adb;
	private AlertDialog ad;

	public InternetChecker(Context context) {
		this.context = context;
	}

	public boolean isConnected() {
		ConnectivityManager connectivity = (ConnectivityManager)
			context.getSystemService(Service.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo info = connectivity.getActiveNetworkInfo();
			if(info != null) {
				if(info.getState() == NetworkInfo.State.CONNECTED) {
					return(true);
				}
			}
		}
		return(false);
	}

	public void notifNoConnection(Context ctx) {
		AlertDialog.Builder adb = new AlertDialog.Builder(ctx);
		adb.setTitle("Please check your internet connection")
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		AlertDialog ad = adb.create();
		ad.show();
	}
}
