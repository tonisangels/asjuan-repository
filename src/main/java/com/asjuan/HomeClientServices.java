package com.asjuan;

import android.os.Bundle;
import android.widget.TextView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.content.Context;
import android.view.ViewGroup;
import android.view.View;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

public class HomeClientServices extends Fragment
{
	View view;
	Context ctx;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.homeclientservices, container, false);
		ctx = getActivity();
		return(view);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getActivity().setTitle("SERVICES");
	}
}
