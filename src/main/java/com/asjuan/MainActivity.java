package com.asjuan;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.graphics.Color;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LinearLayout layout = new LinearLayout(this);
		ImageView juanSplash = new ImageView(this);
		juanSplash.setImageResource(R.drawable.logosplash);
		layout.addView(juanSplash);
		layout.setBackgroundColor(Color.rgb(21, 192, 242));
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				startActivity(new Intent(getApplicationContext(), SignIn.class));
				finish();
			}
		}, 2300);
		setContentView(layout);
	}
}
