package com.asjuan;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.net.HttpURLConnection;

public class GetResponseFromServer
{
	public static String getResponse(HttpURLConnection urlConnection) {
		StringBuilder sb = new StringBuilder("");
		try {
			InputStream in = urlConnection.getInputStream();
			InputStreamReader isr = new InputStreamReader(in);
			BufferedReader br = new BufferedReader(isr);
			String line = "";
			while((line = br.readLine()) != null) {
				sb.append(line);
			}
			urlConnection.disconnect();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return(sb.toString());
	}
}
