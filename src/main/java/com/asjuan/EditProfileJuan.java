package com.asjuan;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Button;
import android.view.View;
import android.widget.ImageView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.content.Context;
import android.view.ViewGroup;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

public class EditProfileJuan extends Fragment
{
	private EditText fnTextfield;
	private EditText aTextfield;
	private EditText mnTextfield;
	private EditText sTextfield;
	private EditText adTextfield;
	private EditText pTextfield;
	private EditText cpTextfield;
	View view;
	Context ctx;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.edit_profile_juan, container, false);
		ctx = getActivity();
		ImageView juanImage = (ImageView) view.findViewById(R.id.juan_image);
		ImageView galleryicon = (ImageView) view.findViewById(R.id.gallery_icon);
		TextView fullnameLabel = (TextView) view.findViewById(R.id.fullname);
		TextView ageLabel = (TextView) view.findViewById(R.id.age);
		TextView mobileNumberLabel = (TextView) view.findViewById(R.id.mobile_number);
		TextView numberLabel = (TextView) view.findViewById(R.id.number);
		TextView skillsLabel = (TextView) view.findViewById(R.id.skills);
		TextView addressLabel = (TextView) view.findViewById(R.id.address);
		TextView passwordLabel = (TextView) view.findViewById(R.id.password);
		TextView confirmPasswordLabel = (TextView) view.findViewById(R.id.confirm_password);
		fnTextfield = (EditText) view.findViewById(R.id.fullname_textfield);
		aTextfield = (EditText) view.findViewById(R.id.age_textfield);
		mnTextfield = (EditText) view.findViewById(R.id.mobile_number_textfield);
		sTextfield = (EditText) view.findViewById(R.id.skills_textfield);
		adTextfield = (EditText) view.findViewById(R.id.address_textfield);
		pTextfield = (EditText) view.findViewById(R.id.password_textfield);
		cpTextfield = (EditText) view.findViewById(R.id.confirm_password_textfield);
		juanImage.setImageResource(R.drawable.juan_image);
		galleryicon.setImageResource(R.drawable.gallery_icon);
		return(view);
	}

	public void signingUp(View view) {
		String fnString = fnTextfield.getText().toString().trim();
		String aString = aTextfield.getText().toString().trim();
		String mnString = mnTextfield.getText().toString().trim();
		String sString = sTextfield.getText().toString().trim();
		String adString = adTextfield.getText().toString().trim();
		String pString = pTextfield.getText().toString().trim();
		String cpString = cpTextfield.getText().toString().trim();
		if(!fnString.isEmpty() && !aString.isEmpty() && !mnString.isEmpty() && !sString.isEmpty() && !adString.isEmpty() && !pString.isEmpty() && !cpString.isEmpty()) {
			Toast.makeText(ctx, "Saved", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getActivity().setTitle("Edit Profile");
	}
}
