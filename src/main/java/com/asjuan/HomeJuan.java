package com.asjuan;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Intent;
import android.widget.ScrollView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.content.Context;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.app.FragmentTransaction;
import android.support.annotation.Nullable;

public class HomeJuan extends Fragment
{
	TextView tvh;
	TextView tvb;
	TextView tvdetails;
	View view;
	Context ctx;
	AppCompatActivity aca;

	public HomeJuan() {
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.homejuan, container, false);
		ctx = getActivity();
		aca = (AppCompatActivity)getActivity();
		tvh = (TextView)view.findViewById(R.id.header);
		tvh.setText("SCHEDULE");
		tvb = (TextView)view.findViewById(R.id.body);
		tvb.setText("Time " +"\n" + "Date " + "\n" + "Client Name " + "\n" + "Service " + "\n" + "Address " + "\n");
		tvdetails = (TextView)view.findViewById(R.id.txtview_details);
		tvdetails.setText("09:00 AM " +"\n" + "Aug 24, 2018 " + "\n" + "Juan Dela Cruz " + "\n" + "Aircon Cleaning " + "\n" + "#321 Balibago Sta. Rosa " + "\n");
		ImageView ajob = (ImageView)view.findViewById(R.id.juanimg_ajob);
		ImageView rjob = (ImageView)view.findViewById(R.id.juanimg_rjob);
		rjob.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				aca.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SummaryOfServices()).commit();
			}
		});
		ajob.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				aca.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AvailableJobs()).commit();
			}
		});
		ajob.setImageResource(R.drawable.ajob);
		rjob.setImageResource(R.drawable.rjob);
		return(view);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getActivity().setTitle("HOME");
	}
}
