package com.asjuan;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Intent;
import android.widget.ScrollView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.content.Context;
import android.view.ViewGroup;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

public class HomeClient extends Fragment
{
	TextView tvs;
	TextView tvrs;
	TextView tvn;
	TextView tvreqserv;
	View view;
	Context ctx;
	AppCompatActivity aca;

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.homeclient, container, false);
		ctx = getActivity();
		aca = (AppCompatActivity)getActivity();
		tvs = (TextView)view.findViewById(R.id.txtview_services_icon);
		tvs.setText("SERVICES");
		tvrs = (TextView)view.findViewById(R.id.txtview_requestedservice);
		tvrs.setText("REQUESTED SERVICES");
		tvn = (TextView)view.findViewById(R.id.txtview_notification_icon);
		tvn.setText("NOTIFICATION");
		tvreqserv = (TextView)view.findViewById(R.id.txtview_requestserv);
		tvreqserv.setText("REQUEST SERVICE");
		ImageView s = (ImageView)view.findViewById(R.id.clientimg_services_icon);
		ImageView rs = (ImageView)view.findViewById(R.id.clientimg_requestedservices);
		ImageView n = (ImageView)view.findViewById(R.id.clientimg_notification_icon);
		ImageView reqserv = (ImageView)view.findViewById(R.id.clientimg_requestservice);
		s.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				aca.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Services()).commit();
			}
		});
		tvs.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				aca.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Services()).commit();
			}
		});
		rs.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				aca.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new RequestedServices()).commit();
			}
		});
		tvrs.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				aca.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new RequestedServices()).commit();
			}
		});
		reqserv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				aca.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ServiceDetailsClient()).commit();
			}
		});
		tvreqserv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				aca.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ServiceDetailsClient()).commit();
			}
		});
		s.setImageResource(R.drawable.services_icon);
		rs.setImageResource(R.drawable.requestedservices);
		n.setImageResource(R.drawable.notification_icon);
		reqserv.setImageResource(R.drawable.requestservice);
		return(view);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getActivity().setTitle("Home");
	}
}
