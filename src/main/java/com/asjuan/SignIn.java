package com.asjuan;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;
import android.text.method.LinkMovementMethod;
import android.text.SpannableString;
import android.text.style.ClickableSpan;
import android.text.Spanned;
import android.view.View;
import android.widget.ImageView;
import android.widget.EditText;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View.OnClickListener;
import android.content.Context;

public class SignIn extends Activity
{
	private EditText eTextfield;
	private EditText pTextfield;
	private Context context = this;
	private InternetChecker ic;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signin);
		ImageView email = (ImageView) findViewById(R.id.image_email);
		ImageView password = (ImageView) findViewById(R.id.image_password);
		ImageView facebook = (ImageView) findViewById(R.id.image_facebook);
		ImageView twitter = (ImageView) findViewById(R.id.image_twitter);
		TextView signUpText = (TextView) findViewById(R.id.sign_up_link);
		TextView forgotText = (TextView) findViewById(R.id.forgot_link);
		eTextfield = (EditText) findViewById(R.id.email_textfield);
		pTextfield = (EditText) findViewById(R.id.password_textfield);
		SpannableString signUpLink = new SpannableString(getString(R.string.sign_up_text));
		SpannableString forgotLink = new SpannableString(getString(R.string.forgot_password));
		AlertDialog.Builder adb = new AlertDialog.Builder(context);
		ic = new InternetChecker(context);
		ClickableSpan su = new ClickableSpan() {
			@Override
			public void onClick(View view) {
				Intent signUp = new Intent(getApplicationContext(), SignUp.class);
				startActivity(signUp);
			}
		};
		ClickableSpan fp = new ClickableSpan() {
			@Override
			public void onClick(View view) {
				if (ic.isConnected()) {
					final EditText email = new EditText(context);
					AlertDialog.Builder adb = new AlertDialog.Builder(context);
					adb.setTitle("Forgot Password?")
					.setMessage("Enter your email address to request a password change.")
					.setView(email)
					.setPositiveButton("Request", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							Toast.makeText(SignIn.this, "Message was sent to your email address", Toast.LENGTH_LONG).show();
							dialog.dismiss();
						}
					})
					.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});
					AlertDialog ad = adb.create();
					ad.show();
				}
				else {
					ic.notifNoConnection(context);
				}
			}
		};
		signUpLink.setSpan(su, 23, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		signUpText.setText(signUpLink);
		signUpText.setMovementMethod(LinkMovementMethod.getInstance());
		forgotLink.setSpan(fp, 0, getString(R.string.forgot_password).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		forgotText.setText(forgotLink);
		forgotText.setMovementMethod(LinkMovementMethod.getInstance());
		email.setImageResource(R.drawable.email);
		password.setImageResource(R.drawable.password);
		facebook.setImageResource(R.drawable.facebook);
		twitter.setImageResource(R.drawable.twitter);
	}

	public void signingIn(View view) {
		String eString = eTextfield.getText().toString();
		String pString = pTextfield.getText().toString();
		if (ic.isConnected()) {
			String[] details = {
				"login",
				eString,
				pString
			};
			new HttpClient("https://asjuan2018.appspot.com/login", context).execute(details);
		}
		else {
			ic.notifNoConnection(context);
		}
	}
}
