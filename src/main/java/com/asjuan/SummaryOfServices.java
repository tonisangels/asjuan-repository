package com.asjuan;

import android.os.Bundle;
import android.widget.ListView;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.view.View;
import android.widget.Toast;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;

public class SummaryOfServices extends Fragment
{
	ListView listView;
	View view;
	Context ctx;
	AppCompatActivity aca;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.summaryofservices, container, false);
		ctx = getActivity();
		aca = (AppCompatActivity)getActivity();
		listView = (ListView)view.findViewById(R.id.list);
		String[] values = new String[] { "Refrigerator Services - 08/08/18", "Laundry Services - 08/08/18", "Automotive and Electrical Services - 08/08/18", "Oxygen Acetyline Welding Services - 08/08/18", "Shielded Metal Arch Services - 08/08/18", "Bench Work and Drilling Services - 08/08/18", "Industrial Electricity Services - 08/08/18", "Machine Shop Tooling and Fabrication Services - 08/08/18", "asdasd - 08/08/18", "asdasdasdasd - 08/08/18", "wewewe - 08/08/18", "gafafa - 08/08/18", "adawdfsdgsg - 08/08/18", "adasg - 08/08/18" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, android.R.id.text1, values);
		listView.setAdapter(adapter); 
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			int itemPosition = position;
			String itemValue = (String) listView.getItemAtPosition(position);
			aca.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ServiceDetailsJuan()).commit();
			}
		});
		return(view);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getActivity().setTitle("SUMMARY OF SERVICES");
	}
}
