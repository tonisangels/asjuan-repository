package com.asjuan;

import android.os.Bundle;
import android.widget.TextView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.content.Context;
import android.view.ViewGroup;
import android.view.View;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.content.Intent;

public class RequestedServices extends Fragment
{
	TextView requestedservice;
	TextView datetime;
	TextView dtvalue;
	TextView details;
	View view;
	Context ctx;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.requestedservices, container, false);
		ctx = getActivity();
		requestedservice = (TextView)view.findViewById(R.id.requested_service);
		requestedservice.setText("Refrigerator Service");
		datetime = (TextView)view.findViewById(R.id.requested_service_date);
		datetime.setText("Date/Time:");
		dtvalue = (TextView)view.findViewById(R.id.requested_service_date_value);
		dtvalue.setText("08/29/2018 | 04:30pm");
		details = (TextView)view.findViewById(R.id.requested_service_details);
		details.setText("Details:");
		return(view);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getActivity().setTitle("REQUESTED SERVICES");
	}
}
