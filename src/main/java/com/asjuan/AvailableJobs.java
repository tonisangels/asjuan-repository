package com.asjuan;

import android.os.Bundle;
import android.widget.ListView;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.view.View;
import android.widget.Toast;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.Button;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;

public class AvailableJobs extends Fragment
{
	View view;
	ListView listView;
	Context ctx;
	AppCompatActivity aca;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.availablejobs, container, false);
		ctx = getActivity();
		aca = (AppCompatActivity)getActivity();
		listView = (ListView) view.findViewById(R.id.list);
		String[] values = new String[] { "Refrigerator Services", "Laundry Services", "Automotive and Electrical Services", "Oxygen Acetyline Welding Services", "Shielded Metal Arch Services", "Bench Work and Drilling Services", "Industrial Electricity Services", "Machine Shop Tooling and Fabrication Services", "asdasd", "asdasdasdasd", "wewewe", "gafafa", "adawdfsdgsg", "adasg" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, android.R.id.text1, values);
		listView.setAdapter(adapter); 
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			int itemPosition = position;
			String itemValue = (String) listView.getItemAtPosition(position);
			aca.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new JobDetailsJuan()).commit();
			}
		});
		return(view);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getActivity().setTitle("AVAILABLE JOBS");
	}
}
