package com.asjuan;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.widget.TextView;
import android.view.View;
import android.widget.ImageView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.Button;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;

public class AboutUs extends Fragment
{
	View view;
	Context ctx;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.aboutus, container, false);
		ctx = getActivity();
		TextView aboutus = (TextView)view.findViewById(R.id.paragraph1);
		ImageView aboutuslogo = (ImageView)view.findViewById(R.id.about_us_asjuan_logo);
		ImageView line = (ImageView)view.findViewById(R.id.line);
		ImageView aboutusicons = (ImageView)view.findViewById(R.id.about_us_icon);
		aboutuslogo.setImageResource(R.drawable.about_us_asjuan_logo);
		line.setImageResource(R.drawable.horizontal_line_white);
		aboutusicons.setImageResource(R.drawable.about_us_icons);
		return(view);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getActivity().setTitle("ABOUT US");
	}
}
