package com.asjuan;

import android.os.Bundle;
import android.widget.TextView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.app.ActionBar;
import android.support.v4.app.Fragment;
import java.lang.Object;
import android.support.annotation.Nullable;

public class HomeClientActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
	private DrawerLayout drawer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_client);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		drawer = (DrawerLayout) findViewById(R.id.drawer_layout_client);
		NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_client);
		navigationView.setNavigationItemSelectedListener(this);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
			this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.setDrawerListener(toggle);
		toggle.syncState();
		if(savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeClient()).commit();
		}
	}

	@Override
	public boolean onNavigationItemSelected(@Nullable MenuItem item) {
		switch (item.getItemId()) {
			case R.id.nav_home: {
				getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeClient()).commit();
				break;
			}
			case R.id.nav_editprofileclient: {
				getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new EditProfileClient()).commit();
				break;
			}
			case R.id.nav_about_us: {
				getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AboutUs()).commit();
				break;
			}
			case R.id.nav_contact_us: {
				getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ContactUs()).commit();
				break;
			}
			case R.id.nav_services: {
				getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Services()).commit();
				break;
			}
			case R.id.nav_requestedServices: {
				getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new RequestedServices()).commit();
				break;
			}
			case R.id.nav_logout: {
				finish();
				break;
			}
		}
		drawer = (DrawerLayout) findViewById(R.id.drawer_layout_client);
		drawer.closeDrawer(GravityCompat.START);
		return(true);
	}

	public void changeFragment(Fragment fragment) {
		getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
	}

	@Override
	public void onBackPressed() {
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		}
		else {
			super.onBackPressed();
		}
	}
}
