package com.asjuan;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Button;
import android.view.View;
import android.widget.ImageView;
import android.widget.Button;
import android.text.TextWatcher;
import android.text.Editable;
import android.content.Context;

public class SignUpDetails extends Activity
{
	private EditText fnTextfield;
	private EditText eTextfield;
	private EditText mnTextfield;
	private EditText pTextfield;
	private EditText cpTextfield;
	private Button signupbtn;
	private Context ctx;
	private String usertype;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signupdetails);
		Bundle getUserType = getIntent().getExtras();
		usertype = getUserType.getString("usertype");
		ImageView logoicon = (ImageView) findViewById(R.id.logojuan_icon);
		TextView fullnameLabel = (TextView) findViewById(R.id.fullname);
		TextView emailLabel = (TextView) findViewById(R.id.email);
		TextView mobileNumberLabel = (TextView) findViewById(R.id.mobile_number);
		TextView numberLabel = (TextView) findViewById(R.id.number);
		TextView passwordLabel = (TextView) findViewById(R.id.password);
		TextView confirmPasswordLabel = (TextView) findViewById(R.id.confirm_password);
		ctx = getApplicationContext();
		fnTextfield = (EditText) findViewById(R.id.fullname_textfield);
		eTextfield = (EditText) findViewById(R.id.email_textfield);
		mnTextfield = (EditText) findViewById(R.id.mobile_number_textfield);
		pTextfield = (EditText) findViewById(R.id.password_textfield);
		cpTextfield = (EditText) findViewById(R.id.confirm_password_textfield);
		signupbtn = (Button) findViewById(R.id.sign_up_button);
		logoicon.setImageResource(R.drawable.another_logo);

		signupbtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				boolean isFullnameV = FieldValidator.isFullnameValid(fnTextfield.getText().toString());
				boolean isEmailV = FieldValidator.isEmailValid(eTextfield.getText().toString());
				boolean isNumberV = FieldValidator.isNumberValid(mnTextfield.getText().toString());
				boolean isPasswordV = FieldValidator.isPasswordValid(pTextfield.getText().toString());
				boolean isPAndCPSame = FieldValidator.isConfirmPasswordCorrect(pTextfield.getText().toString(), cpTextfield.getText().toString());
				if(isFullnameV == false) {
					fnTextfield.setError("Invalid fullname");
				}
				if(isEmailV == false) {
					eTextfield.setError("Invalid email");
				}
				if(isNumberV == false) {
					mnTextfield.setError("Invalid mobile number");
				}
				if(isPasswordV == false) {
					pTextfield.setError("Invalid password");
				}
				if(isPAndCPSame == false) {
					cpTextfield.setError("Password not match!");
				}
				if(isFullnameV == true && isEmailV == true && isNumberV == true && isPasswordV == true && isPAndCPSame == true) {
					String[] details = {
						"register",
						fnTextfield.getText().toString(),
						eTextfield.getText().toString(),
						mnTextfield.getText().toString(),
						pTextfield.getText().toString(),
						usertype
					};
					new HttpClient("https://asjuan2018.appspot.com/register", ctx).execute(details);
				}
			}
		});
	}
}
