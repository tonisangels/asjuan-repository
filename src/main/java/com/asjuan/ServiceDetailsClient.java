package com.asjuan;

import android.os.Bundle;
import android.widget.TextView;
import android.app.DatePickerDialog;
import android.view.View;
import java.util.Calendar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.widget.DatePicker;
import android.app.TimePickerDialog;
import android.widget.TimePicker;
import android.text.format.DateFormat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.content.Context;
import android.view.ViewGroup;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class ServiceDetailsClient extends Fragment
{
	private TextView ddate;
	private TextView dtime;
	private DatePickerDialog.OnDateSetListener datelistener;
	private TimePickerDialog.OnTimeSetListener timelistener;
	private Calendar calendar;
	private TimePickerDialog timePickerDialog;
	private Button cfmButton;
	private Button cnlButton;
	View view;
	Context ctx;

	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.servicedetailsclient, container, false);
		ctx = getActivity();
		ddate = (TextView)view.findViewById(R.id.targetdatefield);
		ddate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Calendar c = Calendar.getInstance();
				int year = c.get(Calendar.YEAR);
				int month = c.get(Calendar.MONTH);
				int day = c.get(Calendar.DAY_OF_MONTH);
				DatePickerDialog ddialog = new DatePickerDialog(ctx, datelistener, year, month, day);
				ddialog.show();
			}
		});

		datelistener = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker datePicker, int year, int month, int day) {
				String date = month + "/" + day + "/" + year;
				ddate.setText(date);
			}
		};

		dtime = (TextView)view.findViewById(R.id.targettimefield);
		dtime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Calendar cc = Calendar.getInstance();
				int hour = cc.get(Calendar.HOUR_OF_DAY);
				int minute = cc.get(Calendar.MINUTE);
				TimePickerDialog tdialog = new TimePickerDialog(ctx, timelistener, hour, minute, DateFormat.is24HourFormat(ctx));
				tdialog.show();
			}
		});

		timelistener = new TimePickerDialog.OnTimeSetListener() {
			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				String time = hourOfDay + ":" + minute;
				dtime.setText(time);
			}
		};
		return(view);
	}

	public void toConfirm() {
		AlertDialog.Builder adb = new AlertDialog.Builder(ctx);
		adb.setTitle("Are you sure you want to confirm your request?")
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				Toast.makeText(ctx, "yes", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog ad = adb.create();
		ad.show();
	}
	
	public void toCancel() {
		AlertDialog.Builder adb = new AlertDialog.Builder(ctx);
		adb.setTitle("Are you sure you want to cancel your request?")
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				Toast.makeText(ctx, "yes", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog ad = adb.create();
		ad.show();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getActivity().setTitle("SERVICES DETAILS");
	}
}
