package com.asjuan;

import android.app.Activity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.content.Intent;
import android.widget.TextView;
import android.widget.ImageView;
import android.text.method.LinkMovementMethod;
import android.text.SpannableString;
import android.text.style.ClickableSpan;
import android.text.Spanned;
import android.view.View;

public class SignUp extends Activity
{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signup);
		TextView agreement = (TextView) findViewById(R.id.agreement);
		SpannableString links = new SpannableString(getString(R.string.agreement));
		ClickableSpan tos = new ClickableSpan() {
			@Override
			public void onClick(View view) {
				Intent terms = new Intent(getApplicationContext(), 
					TermsOfService.class);
				startActivity(terms);
			}
		};
		ClickableSpan pp = new ClickableSpan() {
			@Override
			public void onClick(View view) {
				Intent privacy = new Intent(getApplicationContext(), 
					PrivacyPolicy.class);
				startActivity(privacy);
			}
		};
		final CheckBox checkbox = (CheckBox) findViewById(R.id.checkbox);
		ImageView client = (ImageView) findViewById(R.id.image_client);
		ImageView juan = (ImageView) findViewById(R.id.image_juan);
		client.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(checkbox.isChecked()) {
					Intent clientSignUp = new Intent(SignUp.this, SignUpDetails.class);
					clientSignUp.putExtra("usertype", "CLIENT");
					startActivity(clientSignUp);
				}
			}
		});
		juan.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(checkbox.isChecked()) {
					Intent juanSignUp = new Intent(SignUp.this, SignUpDetails.class);
					juanSignUp.putExtra("usertype", "JUAN");
					startActivity(juanSignUp);
				}
			}
		});
		links.setSpan(tos, 32, 48, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		links.setSpan(pp, 53, 67, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		client.setImageResource(R.drawable.client);
		juan.setImageResource(R.drawable.juan);
		agreement.setText(links);
		agreement.setMovementMethod(LinkMovementMethod.getInstance());
	}
}
